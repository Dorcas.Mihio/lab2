package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    ArrayList<FridgeItem> listOfItems= new ArrayList<FridgeItem>();
    int max_size=20;

    @Override
    public int nItemsInFridge() {
        return listOfItems.size();
    }

    @Override
    public int totalSize() {
    return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        boolean placedInOrNot;
        if (nItemsInFridge()<totalSize()){
            listOfItems.add(item);
            placedInOrNot=true;

        }else {
            placedInOrNot=false;

        }
        return placedInOrNot;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (listOfItems.contains(item)) {
            listOfItems.remove(item);
        }else{
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
listOfItems.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood=new ArrayList<>();
        ArrayList<FridgeItem> notYetExpired=new ArrayList<>();
        for(FridgeItem i:listOfItems){
            if(i.hasExpired()){
                expiredFood.add(i);
            }
        }
        listOfItems.removeAll(expiredFood);
        return expiredFood;
    }
}
